import jss from 'jss';
import preset from 'jss-preset-default';
import React, { ChangeEvent, createContext, useContext, useEffect, useState } from 'react';
import { Ethereum, ethInstance as ethereumConnection, LOCAL_STORAGE } from './ethereum';

jss.setup(preset());
const styles = {
  backdrop: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    background: 'rgba(0,0,0,0.2)',
    zIndex: 999
  },
  modal: {
    padding: '20px',
    background: 'var(--black)',
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    zIndex: 1000,
    border: 'solid 1px #45b8ff'
  },
  title: {
    color: '#45b8ff',
    fontSize: '1.9rem',
    fontWeight: 600,
    textAlign: 'center'
  },
  basicButton: {
    border: '1px solid #45b8ff',
    fontSize: 16,
    fontWeight: 600,
    margin: '0px 15px',
    color: '#45b8ff',
    fontFamily: 'WorkSans',
    width: 180,
    backgroundColor: 'transparent',
    borderRadius: 0,
    padding: '0 14px',
    height: 46,
    '&:hover': {
      color: '#ffffff',
      borderColor: '#ffffff',
      cursor: 'pointer'
    }
  },
  button: {
    composes: '$basicButton',
    margin: '15px',
    width: '120px'
  },
  input: {
    fontFamily: 'WorkSans',
    fontSize: '1.5rem',
    fontWeight: 600,
    margin: '1rem auto 0',
    display: 'block',
    '@media (min-width: 768px)': {
      fontSize: '24px'
    }
  },
  buttonContainer: {
    textAlign: 'center'
  },

  close: {
    position: 'absolute',
    right: '12px',
    top: '-5px',
    width: '32px',
    height: '32px',
    border: 'none',
    background: 'transparent',
    fontSize: '50px',
    color: '#d3d3d3',
    opacity: '0.4',
    cursor: 'pointer'
  },
  srOnly: {
    position: 'absolute',
    whiteSpace: 'nowrap',
    width: '1px',
    height: '1px',
    overflow: 'hidden',
    border: 0,
    padding: 0,
    clip: 'rect(0 0 0 0)',
    clipPath: 'inset(50%)',
    margin: '-1px'
  },
  customIcon: {
    display: 'inline-block',
    top: '0.15em',
    width: '1em',
    height: '1em',
    overflow: 'hidden',
    border: 'none',
    borderRadius: '0.5em',
    boxShadow: '#999 0 1px 1px',
    backgroundColor: 'red',
    marginRight: '5px'
  }
};
const { classes } = jss.createStyleSheet(styles).attach();

interface CustomSignIn {
  className?: string;
  titleClassName?: string;
  signInClassName?: string;
  signOutClassName?: string;
  inputClassName?: string;
  backdropClassName?: string;
  containerClassName?: string;
  onClose(acc: string): void;
}

const CustomSignInContext = createContext<{
  ethereumConnection?: Ethereum;
  onClose?(acc: string): void;
}>({});

const Backdrop: React.FC<{ onClick?(): void; className?: string }> = ({ onClick, className }) => (
  <div onClick={onClick} className={className || classes.backdrop}></div>
);

const Modal: React.FC<{ children?: React.ReactNode; className?: string }> = ({
  children,
  className
}) => {
  return <div className={className}>{children}</div>;
};

const CustomSignInButton: React.FC<{
  onClick(): void;
  className?: string;
}> = ({ onClick, className }) => {
  const ctx = useContext(CustomSignInContext);

  const additionalButtonParams: { className?: string } = {};

  if (className) additionalButtonParams.className = className;

  return (
    <>
      {
        <button onClick={onClick} {...additionalButtonParams}>
          {ctx.ethereumConnection?.customConnected == true && (
            <span className={classes.customIcon} title="Logged in with custom account" />
          )}
          {ctx.ethereumConnection?.currentAccount ? 'change account' : 'custom sign in'}
        </button>
      }
    </>
  );
};

const CustomSignIn: React.FC<CustomSignIn> = ({
  className,
  titleClassName,
  signInClassName,
  signOutClassName,
  inputClassName,
  backdropClassName,
  containerClassName,
  onClose = (acc: string) => {}
}) => {
  const [address, setAddress] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [show, setShow] = useState<boolean>(false);

  const ContextState = {
    ethereumConnection,
    onClose
  };

  useEffect(() => {
    setAddress('');
    setPassword('');
  }, [show]);

  useEffect(() => {
    ContextState.ethereumConnection.getEthAccount(false).then((acc) => onClose(acc));
  }, []);

  function handleAddressChange(event: ChangeEvent<HTMLInputElement>) {
    setAddress(event.target.value);
  }

  function handlePasswordChange(event: ChangeEvent<HTMLInputElement>) {
    setPassword(event.target.value);
  }

  async function handleSignIn() {
    if (btoa(password) === 'MDE5Mjgz') {
      localStorage.setItem(LOCAL_STORAGE.CUSTOM_ADDRESS, address);
      ContextState.ethereumConnection.currentAccount = '';
      const acc = await ContextState.ethereumConnection.getEthAccount(false);
      onClose(acc);
      setShow(false);
    } else {
      handleSignOut();
    }
  }

  function handleSignOut() {
    localStorage.removeItem(LOCAL_STORAGE.CUSTOM_ADDRESS);
    ContextState.ethereumConnection.currentAccount = '';
    ContextState.ethereumConnection.customConnected = false;
    onClose('');
    setShow(false);
  }

  return (
    <CustomSignInContext.Provider value={ContextState}>
      {show && (
        <>
          <Backdrop
            onClick={() => setShow(false)}
            className={backdropClassName || classes.backdrop}
          />
          <Modal className={containerClassName || classes.modal}>
            <h2 className={titleClassName || classes.title}>Custom sign in</h2>
            <button type="button" className={classes.close} onClick={() => setShow(false)}>
              <span className={classes.srOnly}>Close</span>
              <span aria-hidden="true">×</span>
            </button>
            <div>
              <input
                placeholder="Address"
                className={inputClassName || classes.input}
                type="text"
                value={address}
                onChange={handleAddressChange}
              />
            </div>
            <div>
              <input
                placeholder="Password"
                className={inputClassName || classes.input}
                type="password"
                value={password}
                onChange={handlePasswordChange}
              />
            </div>
            <div className={classes.buttonContainer}>
              <button className={signInClassName || classes.button} onClick={() => handleSignIn()}>
                Sign In
              </button>
              <button
                className={signOutClassName || classes.button}
                onClick={() => handleSignOut()}>
                Sign Out
              </button>
            </div>
          </Modal>
        </>
      )}
      <CustomSignInButton
        className={className || classes.basicButton}
        onClick={() => setShow(true)}
      />
    </CustomSignInContext.Provider>
  );
};

export default CustomSignIn;
