type Network = {
  key: string;
  name: string;
  shortName?: string;
  chainId: number;
  network: string;
  explorer?: string;
  rpc?: string[];
  nativeCurrency?: {
    name: string;
    symbol: string; // 2-6 characters long
    decimals: 18;
  };
};
const networks: Record<string, Network> = {
  '1': {
    key: '1',
    name: 'Ethereum Mainnet',
    shortName: 'Ethereum',
    chainId: 1,
    network: 'mainnet',
    explorer: 'https://etherscan.io'
  },
  '3': {
    key: '3',
    name: 'Ethereum Testnet Ropsten',
    shortName: 'Ropsten',
    chainId: 3,
    network: 'ropsten',
    explorer: 'https://ropsten.etherscan.io'
  },
  '4': {
    key: '4',
    name: 'Ethereum Testnet Rinkeby',
    shortName: 'Rinkeby',
    chainId: 4,
    network: 'rinkeby',
    explorer: 'https://rinkeby.etherscan.io'
  },
  '5': {
    key: '5',
    name: 'Ethereum Testnet Görli',
    shortName: 'Görli',
    chainId: 5,
    network: 'goerli',
    explorer: 'https://goerli.etherscan.io'
  },
  '7': {
    key: '7',
    name: 'ThaiChain',
    chainId: 7,
    network: 'thaichain',
    explorer: 'https://exp.tch.in.th'
  },
  '10': {
    key: '10',
    name: 'Optimism',
    shortName: 'Optimism',
    chainId: 10,
    network: 'mainnet',
    rpc: ['https://optimism-mainnet.infura.io/v3/091a03fbd2eb499c800e06ef085fb1d2'],
    explorer: 'https://optimistic.etherscan.io'
  },
  '30': {
    key: '30',
    name: 'Rsk Mainnet',
    shortName: 'Rsk Mainnet',
    chainId: 30,
    network: 'orchid',
    explorer: 'https://explorer.rsk.co'
  },
  '31': {
    key: '31',
    name: 'Rsk Testnet',
    shortName: 'Rsk Testnet',
    chainId: 31,
    network: 'orchidTestnet',
    explorer: 'https://explorer.testnet.rsk.co'
  },
  '42': {
    key: '42',
    name: 'Ethereum Testnet Kovan',
    shortName: 'Kovan',
    chainId: 42,
    network: 'kovan',
    explorer: 'https://kovan.etherscan.io'
  },
  '50': {
    key: '50',
    name: 'XinFin MainNet',
    shortName: 'XDC',
    chainId: 50,
    network: 'xinfin',
    explorer: 'http://explorer.xinfin.network/'
  },
  '56': {
    key: '56',
    name: 'Binance Smart Chain Mainnet',
    shortName: 'BSC',
    chainId: 56,
    network: 'bsc',
    explorer: 'https://bscscan.com'
  },
  '61': {
    key: '61',
    name: 'Ethereum Classic Mainnet',
    shortName: 'Ethereum Classic',
    chainId: 61,
    network: 'classic',
    explorer: 'https://blockscout.com/etc/mainnet'
  },
  '99': {
    key: '99',
    name: 'POA Core',
    shortName: 'POA',
    chainId: 99,
    network: 'core',
    explorer: 'https://blockscout.com/poa/core/'
  },
  '82': {
    key: '82',
    name: 'Meter Mainnet',
    shortName: 'Meter',
    chainId: 82,
    network: 'meter',
    explorer: 'https://scan.meter.io'
  },
  '97': {
    key: '97',
    name: 'Binance Smart Chain Testnet',
    shortName: 'BSC Testnet',
    chainId: 97,
    network: 'testnet',
    explorer: 'https://testnet.bscscan.com'
  },
  '100': {
    key: '100',
    name: 'Gnosis Chain',
    shortName: 'Gnosis',
    chainId: 100,
    network: 'mainnet',
    explorer: 'https://gnosisscan.io'
  },
  '10200': {
    key: '10200',
    name: 'Gnosis Chiado Testnet',
    shortName: 'Gnosis Testnet',
    chainId: 10200,
    network: 'testnet',
    explorer: 'https://gnosis-chiado.blockscout.com/'
  },
  '137': {
    key: '137',
    name: 'Matic Mainnet',
    shortName: 'Matic',
    chainId: 137,
    network: 'matic',
    explorer: ''
  },
  '420': {
    key: '420',
    name: 'Optimistic Goerli',
    chainId: 420,
    network: 'mainnet',
    rpc: ['https://optimism-goerli.infura.io/v3/091a03fbd2eb499c800e06ef085fb1d2'],
    explorer: 'https://goerli-optimism.etherscan.io/'
  },
  '32659': {
    key: '32659',
    name: 'Fusion Mainnet',
    chainId: 32659,
    network: 'mainnet',
    explorer: 'https://fsnex.com'
  },
  '80001': {
    key: '80001',
    name: 'Matic Mumbai',
    chainId: 80001,
    network: 'maticMumbai',
    explorer: ''
  },
  wanchain: {
    key: 'wanchain',
    name: 'Wanchain',
    chainId: 1,
    network: 'mainnet',
    explorer: 'https://www.wanscan.org'
  },
  '1337': {
    key: '1337',
    name: 'Localhost 8545',
    shortName: 'localhost',
    chainId: 1337,
    network: 'devChain'
  },
  '17': {
    key: '17',
    name: 'Docker localhost 8545',
    shortName: 'dockerLocalhost',
    chainId: 17,
    network: 'docker localhost'
  },
  '108': {
    key: '108',
    name: 'Thundercore Mainnet',
    chainId: 108,
    network: 'thundercore',
    explorer: 'https://scan.thundercore.com'
  },
  '256': {
    key: '256',
    name: 'Huobi Eco Chain Testnet',
    shortName: 'heco',
    chainId: 256,
    network: 'testnet',
    explorer: 'https://scan-testnet.hecochain.com'
  },
  '128': {
    key: '128',
    name: 'Huobi Eco Chain Mainnet',
    shortName: 'heco',
    chainId: 128,
    network: 'Mainnet',
    explorer: 'https://scan.hecochain.com'
  },
  '43113': {
    key: '43113',
    name: 'Avalanche Fuji Testnet',
    shortName: 'Fuji',
    chainId: 43113,
    network: 'testnet',
    explorer: 'https://testnet.snowtrace.io/'
  },
  '43114': {
    key: '43114',
    name: 'Avalanche C-Chain',
    shortName: 'Avalanche',
    chainId: 43114,
    network: 'Mainnet',
    explorer: 'https://snowtrace.io/'
  },
  '11297108109': {
    key: '11297108109',
    name: 'Palm Mainnet',
    shortName: 'Palm',
    chainId: 11297108109,
    network: 'mainnet',
    explorer: 'https://explorer.palm.io',
    nativeCurrency: {
      name: 'PALM',
      symbol: 'PALM',
      decimals: 18
    }
  },
  '11297108099': {
    key: '11297108099',
    name: 'Palm Testnet',
    shortName: 'Palm test',
    chainId: 11297108099,
    network: 'testnet',
    explorer: 'https://test.palm.epirus.io',
    nativeCurrency: {
      name: 'PALM',
      symbol: 'PALM',
      decimals: 18
    }
  },
  '11155420': {
    key: '11155420',
    name: 'Sepolia Optimism Testnet',
    shortName: 'OP Sepolia',
    chainId: 11155420,
    network: 'testnet',
    rpc: ['https://optimism-sepolia.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405'],
    explorer: 'https://sepolia-optimism.etherscan.io'
  },
  '8453': {
    key: '8453',
    name: 'Base',
    shortName: 'Base',
    chainId: 8453,
    network: 'mainnet',
    explorer: 'https://base.blockscout.com/',
    rpc: ['https://base-mainnet.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405']
  },
  '84532': {
    key: '84532',
    name: 'Base Sepolia',
    shortName: 'Base Sepolia',
    chainId: 84532,
    network: 'testnet',
    explorer: 'https://sepolia.basescan.org/',
    rpc: ['https://base-sepolia.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405']
  }
};

export default networks;
