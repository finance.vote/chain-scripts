import { JsonFragment } from '@ethersproject/abi';
import detectEthereumProvider from '@metamask/detect-provider/dist/index';
import type Portis from '@portis/web3';
import type WalletConnectProvider from '@walletconnect/web3-provider';
import { WalletLinkConnector } from '@web3-react/walletlink-connector';
import {
  BigNumber,
  BigNumberish,
  Contract,
  Event,
  EventFilter,
  Signer,
  constants,
  providers,
  utils,
  version
} from 'ethers';
import coinbaseIcon from './assets/coinbase.png';
import liqualityIcon from './assets/liquality.png';
import metamaskIcon from './assets/metamask.png';
import portisIcon from './assets/portis.png';
import walletConnectIcon from './assets/walletconnect.png';
import networks from './configs/networks';
import { EthersErrorCode, Key, Leaf, Tree, WalletProvider } from './types';

const isEthersErrorCode = (err: any): err is EthersErrorCode => {
  return typeof err.code === 'number' || typeof err?.data?.originalError?.code === 'number';
};

console.log('Using ethers version: ', version);

const ERROR_CODES = {
  UNRECOGNIZED_CHAIN: 4902,
  WALLET_CONNECT_WRONG_CHAIN: 997
};

const isWindow = typeof window !== 'undefined';

let ethereum: any;
if (isWindow) {
  ethereum = (window as any).ethereum;
}

const PREFIX = process.env.NEXT_PUBLIC_PREFIX || process.env.REACT_APP_PREFIX;
const DEFAULT_CHAIN_ID =
  process.env.NEXT_PUBLIC_DEFAULT_CHAIN_ID || process.env.REACT_APP_DEFAULT_CHAIN_ID;
const AUTOCONNECT_ENABLED =
  process.env.NEXT_PUBLIC_AUTOCONNECT_ENABLED || process.env.REACT_APP_AUTOCONNECT_ENABLED;
const MAINNET_PROVIDER_URL =
  process.env.NEXT_PUBLIC_MAINNET_PROVIDER_URL || process.env.REACT_APP_MAINNET_PROVIDER_URL;
const BSC_PROVIDER_URL =
  process.env.NEXT_PUBLIC_BSC_PROVIDER_URL || process.env.REACT_APP_BSC_PROVIDER_URL;
const BSC_TESTNET_PROVIDER_URL =
  process.env.NEXT_PUBLIC_BSC_TESTNET_PROVIDER_URL ||
  process.env.REACT_APP_BSC_TESTNET_PROVIDER_URL;
const POLYGON_PROVIDER_URL =
  process.env.NEXT_PUBLIC_POLYGON_PROVIDER_URL || process.env.REACT_APP_POLYGON_PROVIDER_URL;
const RSK_MAINNET_PROVIDER_URL =
  process.env.NEXT_PUBLIC_RSK_MAINNET_PROVIDER_URL ||
  process.env.REACT_APP_RSK_MAINNET_PROVIDER_URL;
const RSK_TESTNET_PROVIDER_URL =
  process.env.NEXT_PUBLIC_RSK_TESTNET_PROVIDER_URL ||
  process.env.REACT_APP_RSK_TESTNET_PROVIDER_URL;
const AVALANCHE_MAINNET_PROVIDER_URL =
  process.env.NEXT_PUBLIC_AVALANCHE_MAINNET_PROVIDER_URL ||
  process.env.REACT_APP_AVALANCHE_MAINNET_PROVIDER_URL;
const AVALANCHE_TESTNET_PROVIDER_URL =
  process.env.NEXT_PUBLIC_AVALANCHE_TESTNET_PROVIDER_URL ||
  process.env.REACT_APP_AVALANCHE_TESTNET_PROVIDER_URL;
const MUMBAI_PROVIDER_URL =
  process.env.NEXT_PUBLIC_MUMBAI_PROVIDER_URL || process.env.REACT_APP_MUMBAI_PROVIDER_URL;
const ROPSTEN_PROVIDER_URL =
  process.env.NEXT_PUBLIC_ROPSTEN_PROVIDER_URL || process.env.REACT_APP_ROPSTEN_PROVIDER_URL;
const RINKEBY_PROVIDER_URL =
  process.env.NEXT_PUBLIC_RINKEBY_PROVIDER_URL || process.env.REACT_APP_RINKEBY_PROVIDER_URL;
const GOERLI_PROVIDER_URL =
  process.env.NEXT_PUBLIC_GOERLI_PROVIDER_URL || process.env.REACT_APP_GOERLI_PROVIDER_URL;
const LOCAL_PROVIDER_URL =
  process.env.NEXT_PUBLIC_LOCAL_PROVIDER_URL || process.env.REACT_APP_LOCAL_PROVIDER_URL;
const AVALANCHE_PROVIDER_URL =
  process.env.NEXT_PUBLIC_AVALANCHE_PROVIDER_URL || process.env.REACT_APP_AVALANCHE_PROVIDER_URL;
const OPTIMISM_PROVIDER_URL =
  process.env.NEXT_PUBLIC_OPTIMISM_PROVIDER_URL || process.env.REACT_APP_OPTIMISM_PROVIDER_URL;
const OPTIMISM_GOERLI_PROVIDER_URL =
  process.env.NEXT_PUBLIC_OPTIMISM_GOERLI_PROVIDER_URL ||
  process.env.REACT_APP_OPTIMISM_GOERLI_PROVIDER_URL;
const PALM_MAINNET_PROVIDER_URL =
  process.env.NEXT_PUBLIC_PALM_MAINNET_PROVIDER_URL ||
  process.env.REACT_APP_PALM_MAINNET_PROVIDER_URL;
const PALM_TESTNET_PROVIDER_URL =
  process.env.NEXT_PUBLIC_PALM_TESTNET_PROVIDER_URL ||
  process.env.REACT_APP_PALM_TESTNET_PROVIDER_URL;
const GNOSIS_PROVIDER_URL =
  process.env.NEXT_PUBLIC_GNOSIS_PROVIDER_URL || process.env.REACT_APP_GNOSIS_PROVIDER_URL;
const GNOSIS_TESTNET_PROVIDER_URL =
  process.env.NEXT_PUBLIC_GNOSIS_TESTNET_PROVIDER_URL ||
  process.env.REACT_APP_GNOSIS_TESTNET_PROVIDER_URL;
const BASE_TESTNET_SEPOLIA_PROVIDER_URL =
  process.env.NEXT_PUBLIC_BASE_TESTNET_SEPOLIA_PROVIDER_URL ||
  process.env.REACT_APP_BASE_TESTNET_SEPOLIA_PROVIDER_URL;
const BASE_PROVIDER_URL =
  process.env.NEXT_PUBLIC_BASE_PROVIDER_URL || process.env.REACT_APP_BASE_PROVIDER_URL;
const OPTIMISM_SEPOLIA_PROVIDER_URL = process.env.NEXT_PUBLIC_OPTIMISM_SEPOLIA_PROVIDER_URL || process.env.REACT_APP_OPTIMISM_SEPOLIA_PROVIDER_URL;

const appPrefix: string = PREFIX ? `_${PREFIX}` : '';

export const defaultChainId: number = DEFAULT_CHAIN_ID ? parseInt(DEFAULT_CHAIN_ID) : 1;

export const LOCAL_STORAGE = {
  CURRENT_CHAIN: `currentChain${appPrefix}`,
  CURRENT_PROVIDER: `finance_vote_current_provider${appPrefix}`,
  CUSTOM_ADDRESS: `customAddres${appPrefix}`
};

if (!ethereum) {
  console.warn('Missing ethereum object in the global scope');
}

let autoConnectEnabled = true;
if (AUTOCONNECT_ENABLED === 'false') {
  autoConnectEnabled = false;
}

type DefaultProviders = Record<number, string>;

const defaultEthereumProviders: DefaultProviders = {
  1: MAINNET_PROVIDER_URL || 'wss://mainnet.infura.io/ws/v3/044bfbb71eeb4452a66feb7768d7a1b8', // 'ws://localhost:8545'

  // There should be preferrably wss:// connection instead. You may need to run your own BSC node.
  56: BSC_PROVIDER_URL || 'https://bsc-dataseed.binance.org/', // There are several for your choice: https://docs.binance.org/smart-chain/developer/rpc.html

  97: BSC_TESTNET_PROVIDER_URL || 'https://data-seed-prebsc-1-s1.binance.org:8545', // There are several for your choice: https://docs.binance.org/smart-chain/developer/rpc.html

  137: POLYGON_PROVIDER_URL || 'https://rpc-mainnet.maticvigil.com',

  30: RSK_MAINNET_PROVIDER_URL || 'https://public-node.rsk.co',

  31: RSK_TESTNET_PROVIDER_URL || 'https://public-node.testnet.rsk.co',

  43114: AVALANCHE_MAINNET_PROVIDER_URL || 'https://api.avax.network/ext/bc/C/rpc',

  43113: AVALANCHE_TESTNET_PROVIDER_URL || 'https://api.avax-test.network/ext/bc/C/rpc',

  80001: MUMBAI_PROVIDER_URL || 'wss://rpc-mumbai.matic.today',

  10:
    OPTIMISM_PROVIDER_URL ||
    'https://optimism-mainnet.infura.io/v3/091a03fbd2eb499c800e06ef085fb1d2',

  420:
    OPTIMISM_GOERLI_PROVIDER_URL ||
    'https://optimism-goerli.infura.io/v3/091a03fbd2eb499c800e06ef085fb1d2',

  11297108109:
    PALM_MAINNET_PROVIDER_URL ||
    'https://palm-mainnet.infura.io/v3/091a03fbd2eb499c800e06ef085fb1d2',

  11297108099:
    PALM_TESTNET_PROVIDER_URL ||
    'https://palm-testnet.infura.io/v3/091a03fbd2eb499c800e06ef085fb1d2',

  100:
    GNOSIS_PROVIDER_URL ||
    'https://gnosis-mainnet.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405',

  10200:
    GNOSIS_TESTNET_PROVIDER_URL ||
    'https://gnosis-chiado.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405',

  8453:
    BASE_PROVIDER_URL || 'https://base-mainnet.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405',

  84532:
    BASE_TESTNET_SEPOLIA_PROVIDER_URL ||
    'https://base-sepolia.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405',

  11155420:
    OPTIMISM_SEPOLIA_PROVIDER_URL ||
    'https://optimism-sepolia.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405'
};

if (ROPSTEN_PROVIDER_URL) {
  defaultEthereumProviders[3] = ROPSTEN_PROVIDER_URL;
}

if (RINKEBY_PROVIDER_URL) {
  defaultEthereumProviders[4] = RINKEBY_PROVIDER_URL;
}

if (GOERLI_PROVIDER_URL) {
  defaultEthereumProviders[5] = GOERLI_PROVIDER_URL;
}

if (LOCAL_PROVIDER_URL) {
  defaultEthereumProviders[333] = LOCAL_PROVIDER_URL;
}

if (AVALANCHE_PROVIDER_URL) {
  defaultEthereumProviders[43114] = AVALANCHE_PROVIDER_URL;
}

if (OPTIMISM_GOERLI_PROVIDER_URL) {
  defaultEthereumProviders[420] = OPTIMISM_GOERLI_PROVIDER_URL;
}

if (PALM_MAINNET_PROVIDER_URL) {
  defaultEthereumProviders[11297108109] = PALM_MAINNET_PROVIDER_URL;
}

if (PALM_TESTNET_PROVIDER_URL) {
  defaultEthereumProviders[11297108099] = PALM_TESTNET_PROVIDER_URL;
}

if (OPTIMISM_SEPOLIA_PROVIDER_URL) {
  defaultEthereumProviders[11155420] = OPTIMISM_SEPOLIA_PROVIDER_URL;
}

const zeroHash = '0x0000000000000000000000000000000000000000000000000000000000000000';

const defaultWalletConnectProvider = {
  infuraId: '07e180fc271d4e79a8a9fb1001f57a6b',
  rpc: {
    1: 'https://mainnet.infura.io/v3/07e180fc271d4e79a8a9fb1001f57a6b',
    56: 'https://bsc-dataseed.binance.org',
    137: 'https://rpc-mainnet.maticvigil.com',
    11297108109: defaultEthereumProviders[11297108109],
    11297108099: defaultEthereumProviders[11297108099],
    100:
      GNOSIS_PROVIDER_URL ||
      'https://gnosis-mainnet.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405',
    10200:
      GNOSIS_TESTNET_PROVIDER_URL ||
      'https://gnosis-chiado.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405',
    8453:
      BASE_PROVIDER_URL || 'https://base-mainnet.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405',
    84532:
      BASE_TESTNET_SEPOLIA_PROVIDER_URL ||
      'https://base-sepolia.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405',
    11155420:
      OPTIMISM_SEPOLIA_PROVIDER_URL ||
      'https://optimism-sepolia.blastapi.io/675e3602-fabd-412e-9c3b-fa6f71c82405',
  }
};

const walletProviders: WalletProvider[] = [
  {
    id: 1,
    name: 'MetaMask',
    logo: metamaskIcon.src || metamaskIcon
  },
  {
    id: 2,
    name: 'WalletConnect',
    logo: walletConnectIcon.src || walletConnectIcon
  },
  {
    id: 3,
    name: 'Coinbase',
    logo: coinbaseIcon.src || coinbaseIcon
  },
  {
    id: 4,
    name: 'Liquality',
    logo: liqualityIcon.src || liqualityIcon
  },
  {
    id: 5,
    name: 'Portis',
    logo: portisIcon.src || portisIcon
  }
];

interface WalletRPCProviders extends providers.JsonRpcProvider, providers.Web3Provider {
  enable(): string[];
  isConnecting: boolean;
  request(arg0: { method: string; params: string[] }): unknown;
  chainId?: number;
  isMetaMask?: boolean;
  isPortis?: boolean;
  disconnect(): void;
}

type IReadWeb3Cache = Record<number, WalletRPCProviders | undefined>;
export class Ethereum {
  ethereumProviders?: DefaultProviders;

  readWeb3Cache: IReadWeb3Cache = {};

  writeWeb3?: providers.JsonRpcProvider;
  provider?: WalletRPCProviders | WalletConnectProvider;
  currentAccount?: string;
  customConnected = false;
  currentWalletProvider?: WalletProvider;
  walletLink?: WalletLinkConnector;
  currentChainId = defaultChainId;
  numberOfConnectWalletAttempts = 0;

  constructor(ethereumProviders = defaultEthereumProviders) {
    this.ethereumProviders = ethereumProviders;

    ethereum?.on('accountsChanged', this.handleAccountsChanged.bind(this));
    if (autoConnectEnabled) {
      if (isWindow) {
        window.addEventListener('load', () => this.onLoad());
      }
    }
  }

  async setNewToken(address: string, symbol: string, decimals: number = 18, image?: string) {
    ethereum
      .request({
        method: 'wallet_watchAsset',
        params: {
          type: 'ERC20',
          options: {
            address: address,
            symbol: symbol,
            decimals: decimals,
            image: image
          }
        }
      })
      .then((success: boolean) => {
        if (success) {
          console.log(`${symbol} successfully added to wallet!`);
        } else {
          throw 'Something went wrong.';
        }
      })
      .catch(console.error);
  }

  async onLoadHandler() {
    try {
      const providerId = localStorage.getItem(LOCAL_STORAGE.CURRENT_PROVIDER);
      if (providerId)
        this.currentWalletProvider = walletProviders.find(
          (provider) => provider.id === parseInt(providerId)
        );
    } catch (err) {
      // provider still equals null
    }

    if (this.currentWalletProvider) {
      await this.connectWallet();
      if (!this.provider) return console.error('Provider not found');
      this.writeWeb3 = new providers.Web3Provider(
        this.provider as providers.ExternalProvider,
        'any'
      );
    }
  }

  onLoad() {
    try {
      this.onLoadHandler();
    } catch {
      this.onLoadHandler();
    }
  }

  readWeb3(network?: number) {
    const networkVersion = network || this.getChainId();
    if (!this.ethereumProviders) throw 'No default ethereum providers declared';
    if (
      this.readWeb3Cache[networkVersion as Key<IReadWeb3Cache>] &&
      this.readWeb3Cache instanceof providers.JsonRpcProvider
    ) {
      return this.readWeb3Cache[networkVersion as Key<IReadWeb3Cache>];
    }
    const providerURL = this.ethereumProviders[networkVersion as Key<DefaultProviders>];
    if (!providerURL) {
      console.error(
        'Unsupported network:',
        networkVersion,
        'Please add a node provider url to your .env file if you would like to support this network.'
      );
      return;
    }
    this.readWeb3Cache[networkVersion as Key<IReadWeb3Cache>] = getProvider(
      providerURL
    ) as WalletRPCProviders;

    return this.readWeb3Cache[networkVersion as Key<IReadWeb3Cache>];
  }

  async getContract(
    platform: 'read' | 'write',
    contract: Contract,
    { chainId, account }: { chainId?: number; account?: string } = {}
  ): Promise<Contract> {
    const _chainId = chainId || this.getChainId();
    const contractAddress = contract.networks[_chainId as Key<Contract['networks']>].address;
    if (!contracts[platform][_chainId]) {
      contracts[platform][_chainId] = {};
    }
    if (!contracts[platform][_chainId][contractAddress]) {
      if (platform === 'write') {
        await this.ensureCorrectChainId(await this.getWalletChainId(), _chainId);
        const _account = account || (await this.getEthAccount());
        const web3Contract = await this.getWeb3Contract(
          contract,
          _chainId,
          this.writeWeb3?.getSigner(_account)
        );
        contracts[platform][_chainId][contractAddress] = web3Contract;
      } else if (platform === 'read') {
        const web3Contract = await this.getWeb3Contract(
          contract,
          _chainId,
          this.readWeb3(_chainId)
        );
        contracts[platform][_chainId][contractAddress] = web3Contract;
      }
    }

    return contracts[platform][_chainId][contractAddress];
  }

  validateTxHash(hash: string) {
    return /^0x([A-Fa-f0-9]{64})$/.test(hash);
  }

  async getTransaction(hash: string) {
    if (!this.validateTxHash(hash)) throw 'Wrong transaction hash';

    const provider = this.readWeb3();
    if (!provider) throw 'Provider not found::getTransaction';
    const transaction = await provider.getTransactionReceipt(hash);
    return transaction;
  }

  async getWalletTransactionsHistory(
    account: string,
    abi: string,
    eherscanProvider?: string,
    fromBlock?: number | string,
    toBlock?: number | string
  ) {
    if (!account || !abi) return;

    const _fromBlock = fromBlock ?? 0;
    const _toBlock = toBlock ?? (await this.getBlock('latest'))?.number;

    const etherscanProvider = new providers.EtherscanProvider(eherscanProvider);

    let history = await etherscanProvider.getHistory(account, _fromBlock, _toBlock);

    if (history.length > 0) {
      const results = history
        .filter((x) => x.data != '0x')
        .map(async (x) => {
          const provider = this.readWeb3();
          if (!provider) throw 'Provider not found::getWalletTransactionsHistory';
          const [transaction, receipt] = await Promise.all([
            provider.getTransaction(x.hash),
            provider.getTransactionReceipt(x.hash)
          ]);
          const iface = new utils.Interface([abi]);

          try {
            const decoded = iface.parseTransaction({ data: transaction.data });
            return {
              ...decoded.args,
              status: receipt.status,
              blockNumber: transaction.blockNumber
            };
          } catch (error) {
            console.warn("Transaction doesn't match abi", x.hash);
            return null;
          }
        });
      return await Promise.all(results);
    }
    return [];
  }

  getChainId() {
    return this.currentChainId;
  }

  async getWalletChainId() {
    let chainId = defaultChainId;
    if (!this.provider) throw 'Provider not found::getWalletChainId';
    if (this.provider.chainId && !this.provider.isMetaMask) {
      chainId = parseInt(String(this.provider.chainId));
    } else if (ethereum) {
      chainId = parseInt(await ethereum.request({ method: 'eth_chainId' }), 16);
    }
    return chainId;
  }

  onAccountsChanged(accountsChangedAction: () => void) {
    if (!this.provider) throw 'Provider not found::onAccountsChanged';
    this.provider.on('accountsChanged', () => {
      if (accountsChangedAction) accountsChangedAction();
    });
  }
  async connectWallet() {
    if (!this.currentWalletProvider && this.numberOfConnectWalletAttempts > 3)
      return console.error('No wallet provider chosen');

    const chainId = this.getChainId().toString();

    switch (this.currentWalletProvider?.id) {
      case 1: // METAMASK
        this.provider = (await detectEthereumProvider({
          mustBeMetaMask: true
        })) as WalletRPCProviders;
        if (this.provider) {
          try {
            const account = await ethereum.request({
              method: 'eth_requestAccounts'
            });
            this.handleAccountsChanged(account);
          } catch (err: EthersErrorCode | unknown) {
            if (!isEthersErrorCode(err)) {
              console.error(err);
            } else {
              if (err.code === 4001) {
                // EIP-1193 userRejectedRequest error
                // If this happens, the user rejected the connection request.
                console.log('Please connect to MetaMask.');
              }
              if (err.code === -32002) {
                dispatchEvent(EVENTS.ERROR_METAMASK_ALREADY_IN_PROGRESS);
              }
            }
          }
        } else {
          localStorage.removeItem(LOCAL_STORAGE.CURRENT_PROVIDER);
          console.error('Please select correct wallet provider. Metamask is not found.');
        }
        break;
      case 2: // WALLET CONNECT
        const _WalletConnectProvider: any = (await import('@walletconnect/web3-provider'))
          .default as unknown as WalletConnectProvider;
        // if (this.provider instanceof WalletConnectProvider && this.provider.isConnecting) break;
        const _provider: WalletConnectProvider = new _WalletConnectProvider(
          defaultWalletConnectProvider
        );
        this.provider = _provider ?? undefined;
        if (!this.provider) throw 'Provider not found::WalletConnectProvider';
        try {
          const account = await this.provider.enable();
          if (account?.length) this.handleAccountsChanged(account);
        } catch (err) {
          this.provider.isConnecting = false;
          localStorage.removeItem('finance_vote_current_provider');
          console.log(err);
        }

        this.provider.on('disconnect', () => {
          console.log('disconnected');
          this.handleAccountsChanged([]);
        });
        break;
      case 3:
        this.walletLink = new WalletLinkConnector({
          url: defaultEthereumProviders[parseInt(chainId) as Key<DefaultProviders>],
          appName: 'Finance.vote',
          darkMode: true,
          appLogoUrl: 'https://ipfs.io/ipfs/QmQXeesUyvkBd7ggi6JKN95pkpQznDpjpLFATes9Fss2zq',
          supportedChainIds: Object.keys(defaultEthereumProviders).map((i) => parseInt(i))
        });
        try {
          const { account, provider } = await this.walletLink.activate();
          this.provider = provider;
          if (!account) throw 'Walletlink account not found';
          this.handleAccountsChanged([account]);
        } catch (err) {
          localStorage.removeItem('finance_vote_current_provider');
          console.log(err);
        }
        break;

      case 5:
        try {
          const _Portis: any = (await import('@portis/web3')).default;
          const portis: Portis = new _Portis('10b2f214-40ae-42fe-8aae-bd59b9cec6cd', 'mainnet');
          this.provider = portis.provider;
          const accounts = await this.provider?.enable();
          if (!accounts?.length) throw 'Portis account/s not found';
          this.handleAccountsChanged(accounts);
        } catch (err) {
          localStorage.removeItem('finance_vote_current_provider');
          console.log(err);
        }
        break;

      case 4: // Liquality
        this.provider = (await detectEthereumProvider()) as WalletRPCProviders;
        if (this.provider) {
          await ethereum
            .request({ method: 'eth_requestAccounts' })
            .then(this.handleAccountsChanged.bind(this))
            .catch((err: unknown) => {
              console.error(err);
            });
        } else {
          localStorage.removeItem(LOCAL_STORAGE.CURRENT_PROVIDER);
          console.error('Please select correct wallet provider. Liquality Wallet is not found.');
        }
        break;
    }
  }

  async disconnectWallet() {
    localStorage.removeItem(LOCAL_STORAGE.CUSTOM_ADDRESS);
    if (!this.currentWalletProvider) return console.error('No wallet provider chosen');
    switch (this.currentWalletProvider?.id) {
      case 2:
        await this.disconnectWalletConnect();
        break;
      default:
        // METAMASK
        localStorage.removeItem(LOCAL_STORAGE.CURRENT_PROVIDER);
        this.currentWalletProvider = undefined;
        if (this.customConnected) {
          this.customConnected = false;
        }
        this.handleAccountsChanged([]);
    }
  }

  // type taken from Ethers.
  signPersonalMessageByWallet = async (signParams: Array<string>) => {
    if (!this.provider) {
      throw new Error(`provider hasn't been created yet`);
    }

    try {
      if (this.provider.isPortis) {
        return await this.provider.send('personal_sign', signParams);
      } else {
        return await this.provider.request({
          method: 'personal_sign',
          params: signParams
        });
      }
    } catch (error) {
      console.error(error);
      return error;
    }
  };

  async signPersonalMessage(msg: string, address: string) {
    msg = toHex(Buffer.from(msg, 'utf8'));
    switch (this.currentWalletProvider?.id) {
      case 1: // METAMASK
      case 4: // liquality Wallet
        return ethereum.request({
          method: 'personal_sign',
          params: [msg, address]
        });
      case 2: // WALLET CONNECT
        return await this.signPersonalMessageByWallet([msg, address]);
      case 3: // COINBASE WALLET
        return await this.signPersonalMessageByWallet([msg, address]);
      case 5: // PORTIS
        return await this.signPersonalMessageByWallet([msg, address]);
    }
  }

  async ensureCorrectChainId(currentChainId: number, requestedChainId: number) {
    const hexChainId = '0x' + requestedChainId.toString(16).toUpperCase();

    if (!this.currentWalletProvider) return console.error('No wallet provider chosen');

    try {
      if (currentChainId === requestedChainId) {
        return true;
      } else {
        switch (this.currentWalletProvider?.id) {
          case 1: // METAMASK
            await ethereum.request({
              method: 'wallet_switchEthereumChain',
              params: [{ chainId: hexChainId }]
            });
            break;
          case 2: // wallet connect
            const message = `Please change chain from ${currentChainId} to ${requestedChainId} in your wallet`;
            console.error(message);
            throw { code: ERROR_CODES.WALLET_CONNECT_WRONG_CHAIN, message };
          default:
            // OTHER WALLET
            const msg = 'Please select correct network';
            console.error(msg);
            throw msg;
        }
      }
    } catch (error) {
      if (!isEthersErrorCode(error)) {
        console.error('Change network error:', error);
        throw error;
      }

      if (error.code === ERROR_CODES.WALLET_CONNECT_WRONG_CHAIN) {
        throw error;
      }

      if (
        error.code === ERROR_CODES.UNRECOGNIZED_CHAIN ||
        error?.data?.originalError?.code === ERROR_CODES.UNRECOGNIZED_CHAIN
      ) {
        const addEthereumChainParams: {
          chainId: string;
          chainName: string;
          rpcUrls: string[];
          nativeCurrency?: {
            name: string;
            symbol: string;
            decimals: number;
          };
        } = {
          chainId: hexChainId,
          chainName: networks[requestedChainId]?.name,
          rpcUrls: [defaultEthereumProviders[requestedChainId]]
        };

        const nativeCurrency = networks[requestedChainId]?.nativeCurrency;
        if (nativeCurrency?.decimals && nativeCurrency.name && nativeCurrency.symbol) {
          addEthereumChainParams.nativeCurrency = {
            name: nativeCurrency.name,
            symbol: nativeCurrency.symbol,
            decimals: nativeCurrency.decimals
          };
        }

        try {
          await ethereum.request({
            method: 'wallet_addEthereumChain',
            params: [addEthereumChainParams]
          });
        } catch (addError) {
          console.error('Add network error:', addError);
          throw addError;
        }
      }
    }
  }

  async disconnectWalletConnect() {
    await this.provider?.disconnect();
  }

  isWalletConnected() {
    return this.currentAccount != null;
  }

  getCurrentProvider() {
    return this.provider;
  }

  async getEthAccount(connectWallet = true) {
    const customAccount = localStorage.getItem(LOCAL_STORAGE.CUSTOM_ADDRESS);
    if (!this.currentAccount && customAccount) {
      this.currentAccount = customAccount;
      this.customConnected = true;
    }
    if (!this.currentAccount) {
      if (connectWallet) await this.connectWallet();
      if (!this.currentAccount) {
        if (this.numberOfConnectWalletAttempts > 3) {
          const msg =
            'Please ensure that metamask or another browser wallet is installed and activated to use this feature';
          console.error(msg);
        }
        // alert(msg);
        this.numberOfConnectWalletAttempts += 1;
        return '';
      }
    }
    return this.currentAccount;
  }
  // For now, 'eth_accounts' will continue to always return an array
  handleAccountsChanged(accounts: string[]) {
    console.log('accounts', accounts[0]?.toLowerCase());
    if (accounts.length === 0) {
      // MetaMask is locked or the user has not connected any accounts
      this.currentAccount = undefined;
    } else if (this.provider && this.currentWalletProvider) {
      // rewrite provider only when wallet is connected
      this.writeWeb3 = new providers.Web3Provider(
        this.provider as providers.ExternalProvider,
        'any'
      );
      this.currentAccount = accounts[0]?.toLowerCase();
    }

    // Do any other work!
    document.dispatchEvent(new Event('account_changed'));
    if (!accounts[0]) {
      dispatchEvent(EVENTS.ACCOUNTS_DISCONNECTED);
    }
  }

  async getWeb3Contract(
    contractAbi: Contract,
    chainId?: number,
    provider?: providers.Provider | Signer | undefined
  ) {
    const _chainId = chainId || this.getChainId();
    const contractAddress = contractAbi.networks[_chainId as Key<Contract['networks']>].address;
    const web3Contract = new Contract(contractAddress, contractAbi.abi, provider);
    return web3Contract;
  }

  async getReadContractByAddress(contract: Contract, address: string, network?: number) {
    const t = new Contract(address, contract.abi, this.readWeb3(network));
    return t;
  }

  async getWriteContractByAddress(contract: Contract, address: string, network = null) {
    await this.ensureCorrectChainId(await this.getWalletChainId(), network || this.getChainId());

    const account = await this.getEthAccount();
    const t = new Contract(
      address,
      contract.abi,
      (this.writeWeb3 as providers.JsonRpcProvider)?.getSigner(account)
    );
    return t;
  }

  async getAllEvents(
    callback: (event?: Event) => void,
    contractAbi: Contract['abi'],
    eventName: string,
    {
      filter,
      fromBlock,
      toBlock
    }: { filter: []; fromBlock: string | number; toBlock: string | number },
    subscribe = true,
    chainId?: number
  ) {
    const _chainId = chainId || this.getChainId();
    const provider = this.readWeb3Cache[_chainId as Key<IReadWeb3Cache>];
    const contract = await this.getWeb3Contract(
      contractAbi,
      _chainId,
      (provider as providers.JsonRpcProvider)?.getSigner(0)
    );

    const topics = contract.filters[eventName](...filter);
    const _fromBlock = fromBlock ?? 0;
    const _toBlock = toBlock ?? 'latest';
    let events = await contract.queryFilter(topics, _fromBlock, _toBlock);

    if (subscribe) {
      this.subscribeLogEvent(contract, topics, (event) => {
        callback(event);
      });
    }

    return events;
  }

  subscribeLogEvent(
    contract: Contract,
    topics: string | EventFilter,
    callback: (event?: Event) => void
  ) {
    contract.on(topics, (...args) => {
      const event = args[args.length - 1];
      callback(event);
    });
  }

  async isUserManagement(contract: Contract) {
    let isManagement = false;

    if (this.writeWeb3) {
      const accounts = await (this.writeWeb3 as providers.JsonRpcProvider).listAccounts();
      const metamaskAddr = accounts[0];
      const managementAddr = await this.getManagementAddress(contract);

      isManagement = metamaskAddr === managementAddr;
    }
    return isManagement;
  }

  private async getManagementAddress(contract: Contract) {
    const c = await this.getContract('read', contract);
    const mgmt = await c.management();

    return mgmt;
  }

  async getBlock(block: number | string, chainId?: number) {
    const chain = chainId ?? this.getChainId();
    return this.readWeb3(chain)?.getBlock(block);
  }

  getWalletProviders(customProviders?: number[]) {
    const providers = walletProviders.filter((provider) => {
      if (provider.id === 4 && isWindow && window.ethereum && !('isLiquality' in window.ethereum)) {
        return false;
      }

      if (provider.id === 1 && isWindow && !window.ethereum) return false;
      if (customProviders) {
        if (customProviders.includes(provider.id)) return true;
        return false;
      }
      return true;
    });
    return providers;
  }

  async setWalletProvider(provider: WalletProvider) {
    this.currentWalletProvider = provider;
    localStorage.setItem(LOCAL_STORAGE.CURRENT_PROVIDER, String(provider.id));
    await this.connectWallet();
    // try to connect wallet
  }

  // TODO: Should it be made optional? (for the case if we only read blockchain)
  handleChainChanged(chainId: number) {
    // set new write provider after chain changed
    if (this.provider) {
      this.writeWeb3 = new providers.Web3Provider(
        this.provider as providers.ExternalProvider,
        chainId
      );
    } else {
      this.writeWeb3 = this.readWeb3(chainId);
    }

    this.currentChainId = chainId;
  }
}

const listeners: Record<string, [() => void]> = {};

const EVENTS = {
  ACCOUNTS_DISCONNECTED: 'accountsDisconnected',
  ERROR_METAMASK_ALREADY_IN_PROGRESS: 'errorMetamaskAlreadyInProgress',
  NETWORK_CHANGE: 'chainChange'
};

function dispatchEvent(eventName: string, ...args: []) {
  const handlers = listeners[eventName];

  if (handlers) {
    handlers.forEach((handler) => handler(...args));
  }
}

function addEventListener(eventName: string, handler: () => void) {
  let handlers = listeners[eventName];

  if (!handlers) {
    handlers = listeners[eventName] = [() => { }];
  }

  handlers.push(handler);
}

function removeEventListener(eventName: string, handler: () => void) {
  const handlers = listeners[eventName];

  if (handlers) {
    const index = handlers.indexOf(handler);

    handlers.splice(index, 1);
  }
}

export const eventListeners = {
  EVENTS,
  addEventListener,
  removeEventListener
};
/**********************************************************/
/* Handle chain (network) and chainChanged (per EIP-1193) */
/**********************************************************/

// Normally, we would recommend the 'eth_chainId' RPC method, but it currently
// returns incorrectly formatted chain ID values.
// const currentChainId = ethereum?.chainId

const contracts: Record<'read' | 'write', Record<number, Record<string, Contract>>> = {
  read: {},
  write: {}
};

export function getProvider(url: string) {
  const provider = /^http/.test(url)
    ? new providers.JsonRpcProvider(url, 'any')
    : new providers.WebSocketProvider(url);

  return provider;
}

export function stringToBytes(value: string) {
  return utils.formatBytes32String(value);
}

export function bytesToString(bytes: utils.BytesLike) {
  return utils.parseBytes32String(bytes);
}

export function toChecksumAddress(address: string) {
  return utils.getAddress(address);
}

export function fromWei(num: string) {
  return utils.formatEther(num);
}

export function toWei(num: string) {
  return utils.parseEther(num).toString();
}

export function toBN(num: string | number | BigNumberish) {
  return BigNumber.from(num);
}

export function sha3(arg: string) {
  return utils.id(arg);
}

type IHexlifyValue = number | bigint | utils.BytesLike | utils.Hexable;

export function asciiToHex(ascii: IHexlifyValue) {
  return utils.hexlify(ascii);
}

export function toHex(data: IHexlifyValue) {
  return utils.hexlify(data);
}

export function toEthDate(date: Date) {
  return date.getTime() / 1e3;
}

export function fromEthDate(timestamp: number) {
  return new Date(timestamp * 1e3);
}

export function toHexZeroPad(value: utils.BytesLike, length: number) {
  return utils.hexZeroPad(value, length);
}

export function getMaxApprove() {
  return constants.MaxUint256.toString();
}

const IPFS_GATEWAYS: string[] = [
  'factorydao.infura-ipfs.io',
  'gateway.pinata.cloud',
  'ipfs.infura.io',
  'cloudflare-ipfs.com',
  'cf-ipfs.com',
  'ipfs.io',
  'ipfs.fleek.co',
  'dweb.link'
];

export async function ipfsGet(ipfsHash: string, failoverCallback: () => void) {
  for (let i = 0; i < IPFS_GATEWAYS.length; i++) {
    try {
      const url = `https://${IPFS_GATEWAYS[i]}/ipfs/${ipfsHash}`;
      return await fetch(url).then((res) => res.json());
    } catch (er) {
      throw er;
      // console.error(er);
      // try another gateway
    }
  }
  return failoverCallback ? failoverCallback() : false;
}

export const ethInstance = new Ethereum();

export function generateRandomData(numLeaves: number) {
  const web3 = ethInstance.readWeb3();
  if (!web3) throw 'Web3 not defined in fn::generateRandomData';
  const addressLeaves = [{ address: '0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1' }];
  const metadataLeaves = [{ uri: 'blahblahblah', tokenId: '1' }];

  for (let i = 0; i < numLeaves; i++) {
    if (i % 10000 == 0) {
      console.log({ i });
    }
    addressLeaves.push({ address: utils.hexlify(utils.randomBytes(20)) });
    metadataLeaves.push({
      uri: 'ipfs://' + utils.hexlify(utils.randomBytes(20)),
      tokenId: utils.hexlify(utils.randomBytes(20))
    });
  }

  return { addressLeaves, metadataLeaves };
}

export function createMerkleProof(tree: Tree, target: string, targetKey: string): string[] {
  const leaf = tree.nodes.find((x) => x.data[targetKey] == target);
  let x = { ...leaf };
  const proof = [];
  while (x.parentIndex > -1) {
    const parent = tree.nodes[x.parentIndex];
    const flip = x.index === parent.leftChildIndex;
    if (flip) {
      proof.push(tree.nodes[parent.rightChildIndex].hash);
    } else {
      proof.push(tree.nodes[parent.leftChildIndex].hash);
    }
    x = parent;
  }
  return proof;
}

export function getLeafHash(obj: Leaf, hashTypes: string[], hashKeys: string[]): string {
  const abi = new utils.AbiCoder();
  return utils.keccak256(
    abi.encode(
      hashTypes,
      hashKeys.map((x) => obj[x])
    )
  );
}

export function checkMerkleProof(
  proof: string[],
  root: string,
  obj: Leaf,
  hashTypes: string[],
  hashKeys: string[]
): boolean {
  let a = getLeafHash(obj, hashTypes, hashKeys);
  for (let i = 0; i < proof.length; i++) {
    let b = proof[i];
    // console.log('proof step', { a, b })
    let p = parentHash(a, b);
    a = p[0] as string;
  }
  const correct = a === root;
  // console.log({ correct, a, root })
  return correct;
}

export function createMerkleTree(leaves: Leaf | null[], hashTypes: string[], hashKeys: string[]) {
  let numNodes = 0;
  let allNodes = leaves.map((x: Leaf) => {
    const hash = getLeafHash(x, hashTypes, hashKeys);

    return {
      index: numNodes++,
      data: x,
      hash: hash,
      parentIndex: -1,
      leftChildIndex: -1,
      rightChildIndex: -1
    };
  });
  let currentRow = allNodes.map((x: Record<'index', number>) => x.index);
  let parentRow = [];
  let allRows = [];
  while (currentRow.length > 1) {
    console.log({ currentRow });
    // handle case where the tree is not binary by adding zero hash
    if (currentRow.length % 2) {
      allNodes.push({
        index: numNodes++,
        hash: zeroHash,
        data: null,
        parentIndex: -1,
        leftChildIndex: -1,
        rightChildIndex: -1
      });
      currentRow.push(numNodes - 1);
    }
    // loop over current row
    for (let i = 0; i < currentRow.length; i += 2) {
      const [pHash, flip] = parentHash(
        allNodes[currentRow[i]].hash,
        allNodes[currentRow[i + 1]].hash
      );
      const newNode = {
        index: numNodes++,
        hash: pHash,
        data: null,
        parentIndex: -1,
        leftChildIndex: flip ? allNodes[currentRow[i + 1]].index : allNodes[currentRow[i]].index,
        rightChildIndex: flip ? allNodes[currentRow[i]].index : allNodes[currentRow[i + 1]].index
      };
      allNodes[currentRow[i]].parentIndex = numNodes - 1;
      allNodes[currentRow[i + 1]].parentIndex = numNodes - 1;
      allNodes.push(newNode);
      parentRow.push(newNode.index);
    }
    // keep track of rows
    allRows.push(currentRow);
    // move pointer to next row
    currentRow = parentRow;
    // zero out parent
    parentRow = [];
  }
  allRows.push(currentRow);
  return { rows: allRows, nodes: allNodes, root: allNodes[currentRow] };
}

export function parentHash(a: string, b: string) {
  if (a < b) {
    return [utils.keccak256(a + b.slice(2)), false];
  } else {
    return [utils.keccak256(b + a.slice(2)), true];
  }
}

function buf2hex(buffer: Uint8Array) {
  // buffer is an ArrayBuffer
  return [...new Uint8Array(buffer)].map((x) => x.toString(16).padStart(2, '0')).join('');
}
export function ipfsHashToBytes32(ipfsHash: string) {
  const bytes = '0x' + buf2hex(utils.base58.decode(ipfsHash)).slice(4);
  console.log(bytes);
  return bytes;
}

export function bytes32ToIpfsHash(bytes32Hex: string) {
  // Add our default ipfs values for first 2 bytes:
  // function:0x12=sha2, size:0x20=256 bits
  // and cut off leading "0x"
  const hashHex = '1220' + bytes32Hex.slice(2);
  const hashBytes = Buffer.from(hashHex, 'hex');
  const hashStr = utils.base58.encode(hashBytes);
  console.log(hashStr);
  return hashStr;
}

export function getInterface(abi: string | readonly (string | utils.Fragment | JsonFragment)[]) {
  return new utils.Interface(abi);
}

export async function getWalletAddressFromENS(ENSName: string) {
  const providerURL = defaultEthereumProviders[1];
  const provider = new providers.JsonRpcProvider(providerURL); // Mainnet connect
  const walletAddrFormENS = await provider.resolveName(ENSName);
  return walletAddrFormENS;
}
