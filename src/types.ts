export interface WalletProvider {
  id: number;
  name: string;
  logo: string;
}

export interface EthersErrorMessage {
  message: string;
}
export interface EthersErrorCode {
  code: number;
  data?: {
    originalError?: {
      code?: number;
    };
  };
}

export type Key<T> = keyof T;

export type Leaf = Record<string, any | null>;
export type Tree = Record<'nodes', Leaf[]>;
