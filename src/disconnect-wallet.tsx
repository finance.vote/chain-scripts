import jss from 'jss';
import preset from 'jss-preset-default';
import React, { createContext, PropsWithChildren, useContext, useEffect } from 'react';
import { Ethereum, ethInstance as ethereumConnection } from './ethereum';

jss.setup(preset());
const styles = {
  button: {
    border: 'none',
    fontSize: 16,
    fontWeight: 600,
    opacity: 0.7,
    fontFamily: 'WorkSans',
    backgroundColor: 'transparent',
    borderRadius: 0,
    margin: '0 8px',
    cursor: 'pointer',
    transition: 'opacity 200ms',
    '&:hover': {
      opacity: 1,
      cursor: 'pointer'
    }
  },
  img: {
    width: '20px'
  }
};

const { classes } = jss.createStyleSheet(styles).attach();

export interface DisconnectWalletType extends PropsWithChildren {
  onDisconnect?(): void;
  fill?: string;
  icon?: string;
}

const DisconnectWalletContext = createContext<{
  ethereumConnection?: Ethereum;
}>({});

const DisconnectWalletButton: React.FC<PropsWithChildren<{ fill?: string; icon?: string }>> = ({
  fill,
  children,
  icon
}) => {
  const ctx = useContext(DisconnectWalletContext);

  const disconnect = async () => {
    await ctx.ethereumConnection?.disconnectWallet();
  };

  return (
    <>
      <button onClick={disconnect} className={classes.button} title={'Logout'}>
        <div className={classes.img}>
          {icon ? (
            <img src={icon} alt="logout-icon" />
          ) : (
            <svg
              version="1.1"
              id="Capa_1"
              xmlns="http://www.w3.org/2000/svg"
              x="0px"
              y="0px"
              viewBox="0 0 384.971 384.971">
              <path
                fill={fill}
                d="M180.455,360.91H24.061V24.061h156.394c6.641,0,12.03-5.39,12.03-12.03s-5.39-12.03-12.03-12.03H12.03
        C5.39,0.001,0,5.39,0,12.031V372.94c0,6.641,5.39,12.03,12.03,12.03h168.424c6.641,0,12.03-5.39,12.03-12.03
        C192.485,366.299,187.095,360.91,180.455,360.91z"
              />
              <path
                fill={fill}
                d="M381.481,184.088l-83.009-84.2c-4.704-4.752-12.319-4.74-17.011,0c-4.704,4.74-4.704,12.439,0,17.179l62.558,63.46H96.279
        c-6.641,0-12.03,5.438-12.03,12.151c0,6.713,5.39,12.151,12.03,12.151h247.74l-62.558,63.46c-4.704,4.752-4.704,12.439,0,17.179
        c4.704,4.752,12.319,4.752,17.011,0l82.997-84.2C386.113,196.588,386.161,188.756,381.481,184.088z"
              />
            </svg>
          )}
        </div>
        {children}
      </button>
    </>
  );
};

const DisconnectWallet: React.FC<DisconnectWalletType> = ({
  onDisconnect = () => {},
  fill = '#fff',
  icon,
  children
}) => {
  useEffect(() => {
    document.addEventListener('account_changed', onDisconnect);
  }, []);

  const ContextState = {
    ethereumConnection
  };

  return (
    <DisconnectWalletContext.Provider value={ContextState}>
      <DisconnectWalletButton fill={fill} children={children} icon={icon} />
    </DisconnectWalletContext.Provider>
  );
};

export default DisconnectWallet;
